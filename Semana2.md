# Semana 2

Terminos:

## Desencadenar eventos

Emitir eventos desde un componente;

**Tipescript:** se puede abrevisar la declaración de propiedades o variables publicas de la siguiente manera:

En lugar de:

```js

export class algo{

    num1: int;
    num2: int;

    constructor(bla:int, bla2:int){
        this.num1 = bla;
        ...
    }
}
```

Se puede realizar de la siguiente forma:

```js

export class algo{
    constructo(public num1:int, public num2:int)
}

```

Ahora para poder emitir un evento utilizar `EventEmitter`. Ver los cambios hechos en Git

Esto para emitir un evento desde el nodo hijo con `@output` para que el nodo padre pudiera escucharlo y luego esparcir por todos los nodos hijo alguna acción.

En el component hijo ts:

```js
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @HostBinding("attr.class") cssClass = "col-md-4";
  @Output() clicked: EventEmitter<DestinoViaje>; // Para emitir el vento al nodo padre

  constructor() {
    this.clicked = new EventEmitter(); //Instanciar emitter
  }

  ngOnInit(): void {}

  ir() {
    this.clicked.emit(this.destino); //Emitidor
    return false;
  }
}
```

El vento se llama "clicked" y se puede crear con cualquier nombre, como una variable.

En el template el componente padre:

```html

<div class="row">
    <app-destino-viaje
      *ngFor="let d of destinos"
      [destino]="d"
      (clicked) = "elegido($event)"//aqui se escucha el vento emitido
    ></app-destino-viaje>
<div>

```

Y en el "codbehing" del component padre, este es el método accionado con el evento "clicked":

```js
elegido(d: DestinoViaje){
    this.destinos.forEach(function(x) {
      x.setSelected(false);
    });

    d.setSelected(true);
  }

```

> **Notas:**
>
> 1. Wrap significa encapsular
> 2. Para enviar el objeto que generó el evento a la función del template se puede usar el nombre del objeto, en el caso del template del ejemplo `d` o el atajo `$event`.
> 3. Para jugar con los estilos u otros attr según algo en el component ts se puede usar esta directiva `[style.display]="destino.isSelected() ? '' : 'none'"`

## Mas directivas

También podemos usar las directivas `[ngSwitch] = "variable"` con sus respectivos `*ngSwitchCase` y `*ngSwitchDefault` como en C#.

Está también `*ngIf = "condicion"` .

Estas directivas lo que hacen es mostrar la etiqueta html en donde se cuentran si se cumple la condición que tienen.

`ngNonBindable`: Se utiliza para evitar que angular bindee (o interprete) lo que está dentro de la etiqueta que tiene esta directiva si tuviera algo con sintaxis de angular.

Podemos agregar en *ngFor una variable para obtener el index de la iteración de la siguiente forma `*ngFor="let d of arreglop; let i= index"`

Otra practica no común pasar un parametro @Input() con un nombre custom `@input('idx') posicion:int;'` esto lo que hace es que en el componente padre donde se origina el hijo se va a asignar el valor a la variable idx y ya en el hijo se le asignará el valor a posicion;

## Ruteo (Routing)

Es decir, poder tener distintos componentes que se renderean directamente sobre la vista principal de la aplicación y poder navegar a través de los mismos. No solamente tener un componente principal que renderea todo un árbol de componentes dentro, sino que ese componente raíz, el primer componente, el primer componente que renderea pueda ir cambiando. Y, de esa manera, darle una sensación de que conservamos un marco dentro de la aplicación donde siempre permanece constante pero el contenido va cambiando. Bien. Entonces, para eso, vamos a dirigirnos al archivo de módulo.

Debemos importar el componente para las rutas con `import {RouteModule, Routes} from '@angular/router'`

Y luego agregar las rutas dentro de la clase en `app.module.ts`:

```js
    const routes: Routes = [
        {path: '', redirectTo: 'home', pathMatch: 'full'}
        {path: 'home', component: ListaDestinosComponent}
        {path: 'destino', component: ListaDestinosComponent}
    ]

```

En el imports agreamos las rutas definidas arriba:

```js
imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)//Pasamos el arreglo de rutas para que sean utilizadas en los modulos
  ],
```

## Implementación de formularios interactivos

Debemos agregar un nuevo import FormsModule, y ReactiveFormsModules, esto en el app.module.ts:

```js
...
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; //Agregamos esto en la cabecera
...
  imports: [
    BrowserModule,
    FormsModule,//este
    ReactiveFormsModule,//este
    RouterModule.forRoot(routes)
  ],
  ...
```

Generear un compoement FormDestinoViaje.

Copiar el formulario al componente recien creado y se agregará el componente a donde estaba antiguamente el formulario.

Agregar un evento `(onItemAdded) = "agregado($event)"` en el tag del componente que ahora tiene el formulario. Esto se hace en el componente padre donde esá dicho tag, así:

```html
...
<app-form-destino-viaje
  (onItemAdded)="agregado($event)"
></app-form-destino-viaje>
...
```

En el formulario utilizaremos un form group y asociasremos los inputs a este form group de la siguiente forma:

```html
<form
  [formGroup]="fg"  <--  Nombre del  form group
  (ngSubmit)="guardar(fg.controls['nombre'].value, fg.controls['url'].value)"
>
  <-- Evento cuando se presione el botón submit
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input
      type="text"
      class="form-control"
      id="nombre"
      placeholder="Ingresar nombre..."
      [formControl]="fg.controls['nombre']"<--  Se  asocia   el   input   con   el   form    group
    />
  </div>
  <div class="form-group">
    <label for="Imagen Url">Imagen Url</label>
    <input
      type="text"
      class="form-control"
      id="imagenUrl"
      placeholder="Ingresar url..."
      [formControl]="fg.controls['url']"<--  Se  asocia   el   input   con   el   form    group
    />
  </div>
  <button type="submit" class="btn btn-primary">Guardar!</button>
</form>
```

El lado del "controlador" se debe instanciar e inicializar el form group y untilizar una propiedad FormBuilder para crear los controles para el form group:

```js
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms"; // Se importan los componentes
import { DestinoViaje } from "../models/destino-viaje.models";

@Component({
  selector: "app-form-destino-viaje",
  templateUrl: "./form-destino-viaje.component.html",
  styleUrls: ["./form-destino-viaje.component.css"],
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>; // Se agrega el emitter para pasarlo al componente padre
  fg: FormGroup; //Se instancia el form group

    // Se crea una propiedad de tipo FormBuilder
  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter(); //instancia le emitter
    this.fg = fb.group({
      //Se agregan los controls al f. g.
      nombre: [""],
      url: [""],
    });

    this.fg.valueChanges.subscribe((form: any) => {
      // Esto permite realizar algo cuando cambia los valores del formulaio
      console.log("cambio el formulario: ", form); //En este caso escribe en consola esto
    });
  }

  ngOnInit(): void {}
}
```

**Validadores:** Permite realizar validaciones de datos antes del submit

En la parte del componente debes agregar los siguiente para que el campo sea requerido:

```js
import { FormBuilder, FormGroup, Validators } from '@angular/forms';// Se agrega el Validators al import arriba

 constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.required],// Se agrega esto en el f.g.
      url: ['']
    })
```

Del lado de la vista o template se pueden agregar las siguientes directivas para mostrar un mensaje si es necesario:

```html
<form
  [formGroup]="fg"
  (ngSubmit)="
        guardar(fg.controls['nombre'].value, fg.controls['url'].value)
      "
>
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input
      type="text"
      class="form-control"
      id="nombre"
      placeholder="Ingresar nombre..."
      [formControl]="fg.controls['nombre']"
    />
    <div class="text-danger" *ngIf="!fg.controls['nombre'].valid"> <<--Para validar en general todas la validacioanes que tiene ese campo>>
      Nombre invalido
    </div>
    <div class="text-danger" *ngIf="fg.controls['nombre'].hasError('required')"> <<--Para validar un validador especifico en este caso required>> Nombre
      requerido
    </div>
  </div>
  <div class="form-group">
    <label for="Imagen Url">Imagen Url</label>
    <input
      type="text"
      class="form-control"
      id="imagenUrl"
      placeholder="Ingresar url..."
      [formControl]="fg.controls['url']"
    />
  </div>
  <button type="submit" class="btn btn-primary" *ngIf="fg.valid"><<--Para validar en general todo el formulairio(Si todos los campos superaron la validación)>> 
    Guardar!
  </button>
</form>
```

**Validadores personalizados:**

Podemos agregar validadores personalizados de la siguiente manera:

En el template o vista:

```html
...
<div class="text-danger" *ngIf="!fg.controls['nombre'].hasError('required')">
  Nombre requerido
</div>
<div
  class="text-danger"
  *ngIf="fg.controls['nombre'].hasError('invalidNombre')"
>
  Nombre invalido
</div>
<div
  class="text-danger"
  *ngIf="fg.controls['nombre'].hasError('minLongNombre')"
>
  Nombre debetener al menos {{minLongitud}} de largo
</div>
...
```

Y en el componente va lo siguiente:

```js

 this.fg = fb.group({
    nombre: [
        '',
        Validators.compose([
          Validators.required,
          this.nombreValidator,//Se agrega los validadores personalizados
          this.nombreValidatorParametrizable(this.minLongitud),//Asi se agregan los parametrizables
        ]),
     ],
    url: [''],
});

//Validador sin parametros
nombreValidator(control: FormControl): { [s: string]: boolean } {//Agregamos una funcion validador que debe retornar un objeto json
    const l = control.value.toString().trim().length;

    if (l > 0 && l < 5) {//Validacion
      return { invalidNombre: true };//Retorna el objeto json nota: y usar espacios de nombre de la propiedad debe ir entre ''
    }

    return null;//null si se superó todas la validaciones
}

//Validador con parameters
nombreValidatorParametrizable(minLong: number): ValidatorFn {//Retorna tipo ValidatorFn
    return (control: FormControl): { [s: string]: boolean } | null => {//Función lambda que retorna lo mismo que arriba o un null
      const l = control.value.toString().trim().length;

      if (l > 0 && l < minLong) {//Validacion con el parametro
        return { minLongNombre: true };//Devuelve el objeto
      }

      return null;//Pasó la validación
    };
}

```

El métedo debe retornar un objet tipo JSON de la siguiente forma: {nombrevalidador: true/null} true si la validacion no se supero y null si todo ok!.
Tambien podemos agregar validadores parametrizables como se muestra arriba la función tiene que devolver un objeto de tipo `ValidatorFn`.

> **Nota:** En una función de validación se pueden hacer muchas validaciones y se va cambiando el nombre de la propiedad que retorna para que coincida con el nombre que se colocó en el template asi:

```js

nombreValidatorParametrizable(minLong: number): ValidatorFn {//Retorna tipo ValidatorFn

    if (l > 0 && l < 5) {//Validacion
      return { invalidNombre: true };//nombre de validacion
    }

    return (control: FormControl): { [s: string]: boolean } | null => {//Función lambda que retorna lo mismo que arriba o un null
      const l = control.value.toString().trim().length;

      if (l > 0 && l < minLong) {//Validacion con el parametro
        return { minLongNombre: true };//Nombre diferente de validacion
      }

      return null;//Pasó la validación
    };
}

```

## Integración de angular con Redux

### Principios de diseño de Observables y RxJS
Programación reactiva: hacer lo siguiente.

En este ejemplo vamos a crear una clase que sera nuestro cliente de un api de la siguiente forma:

```js
import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.models';

export class DestinosApiClient {
  destinos: DestinoViaje[];
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);//Obserbable

  constructor() {
    this.destinos = [];
  }

  add(d: DestinoViaje) {
    this.destinos.push(d);
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  getById(id: String): DestinoViaje {
    return this.destinos.filter(function(d) { return d.id.toString() === id;} )[0];
  }

  elegir(d: DestinoViaje) {//Agrega el valor al observable "d" es el nuevo valor
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);
  }

  subcribeOnChance(fn){/// Permite que otros componentes de la aplicación se sucriban 
  /// paralas actualizaciones 
    this.current.subscribe(fn);
  }
}

```

El objeto `current` utiliza un `subject` para crear un obsebable que propagará por toda la aplicación los cambios que haya en el destino seleccionado como favorito o algo así esto es gracias a `BehaviorSubject` que sería como el obserbable. En pocas palabras detecta cuando se cambia el destino favorito.

En esta clase de hará el procesamiento de datos y se consumirá desde el componente de lista-destinos.

Se debe agregar lo siguiente a el `app.module.ts`:

```js
imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule//este
  ],
  providers: [
    DestinosApiClient//este
  ],
```
En el componente `lista-destino.component.ts` se debo modificar la progra para utilizar el `destinos-api-client.ts` y suscribirse para detectar cambios en el destino seleccionado como el preferido:
```js
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  // destinos: DestinoViaje[];
  updates: string[];
//Se agraga la propiedad destinoApiClient
  constructor(public destinoApiClient: DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinoApiClient.subcribeOnChance((d: DestinoViaje) => {//Nos suscribimos para detectar los cambio
    // y agregarlos a una lista
      if (d != null) {
        this.updates.push('Se elegido como principal: ' + d.nombre);
      }
    });
  }

  ngOnInit(): void {}

  // agregado(d: DestinoViaje): boolean {
  //   this.destinos.push(d);
  //   console.log(this.destinos);
  //   return false;
  // }

  agregado(d: DestinoViaje): boolean {
    this.destinoApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  // elegido(d: DestinoViaje) {
  //   this.destinos.forEach(function (x) {
  //     x.setSelected(false);
  //   });

  //   d.setSelected(true);
  // }

  elegido(e: DestinoViaje){
    this.destinoApiClient.elegir(e);
  }
}
```

En el template se sustituira lo siguiente:

```html
<div class="row">
  <!-- Ahora utilizamos el api aqui -->
    <app-destino-viaje
      *ngFor="let d of destinoApiClient.getAll(); let i = index"
      [idx]="i + 1"
      [destino]="d"
      (clicked)="elegido($event)"
    ></app-destino-viaje>
  </div>
  <hr class="mb-5" />
  <div class="row">
    <ul>
      <!-- Utilizar el update para mostrar un historial de cambios en los preferidos -->
      <li *ngFor="let u of updates">{{u}}</li>
    </ul>
  </div>
```

### Programación Reactiva
Vamos a indagar un poco unos aspectos un poquito más avanzados de observables, que es una API muy amplia, que tiene un montón de funcionalidad. Vamos ahora a indagar una parte un poco más avanzada. Primero, vamos a lo que es un ejemplo de rendereo observable de HTML, rendereo diferido de HTML a través de intervalos.

Utilizaremos una formula de fecha con `async`:

En el `app.component.html`:
```js
<hr class="mb-5">

Fecha: {{time | async}}
```
En el componente:
```js
export class AppComponent {
  title = 'angular-wishlist';
  //Agregar esto
  time = new Observable((observer) => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  });
```
Este tipo de obsevables pueden se muy utiles para hacer consultas a un servidor para obtener información. como por ejemplo: **Consultar las coordenadas para reglejar la posición dentro de un mapa en la página**.

En el código del formulario hay que implementar un tipo de autocompletado de la siguiente forma:

**Pipe:** Para eso vamos a usar la función "PIPE", que nos permite hacer un "pipeline", es decir, un flujo de una operación secuencial tras otra, operaciones en serie.

  Ver los comentarios en el código directamente.