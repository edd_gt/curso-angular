import { v4 as uuid } from 'uuid';

export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];
  public id = uuid();

  constructor(
    public nombre: string,
    public imagenurl: string,
    public votes = 0
  ) {
    this.servicios = ['Pileta', 'Desayuno'];
    this.selected = false;
  }

  voteUp(): any {
    this.votes++;
  }

  voteDown(): any {
    this.votes--;
  }

  restartVotes(): any {
    this.votes = 0;
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(op: boolean) {
    this.selected = op;
  }
}
