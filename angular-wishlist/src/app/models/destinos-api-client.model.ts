import { forwardRef, Inject, Injectable } from '@angular/core';
import { DestinoViaje } from './destino-viaje.models';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
  DestinosViajesState,
  NuevoDestinoAction,
  ElegidoFavoritoAction,
} from './destinos-viajes-state.model';
import { AppConfig, AppState, APP_CONFIG, db } from './../app.module';
import {
  HttpRequest,
  HttpHeaders,
  HttpClient,
  HttpEvent,
  HttpResponse,
} from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
    this.store
      .select((state) => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store.subscribe((data) => {
      console.log('all store');
      console.log(data);
    });
  }

  getById(id: String): DestinoViaje {
    return this.destinos.filter(function (d) {
      return d.id.toString() == id;
    })[0];
  }

  add(d: DestinoViaje) {
    // this.destinos.push(d);
    // this.store.dispatch(new NuevoDestinoAction(d));
     const headers: HttpHeaders = new HttpHeaders({
       'X-API-TOKEN': 'token-seguridad',
     });
     const req = new HttpRequest(
       'POST',
       this.config.apiEndpoint + '/my',
       { nuevo: d.nombre },
       { headers: headers }
     );
     this.http.request(req).subscribe((data: HttpResponse<{}>) => {
       if (data.status === 200) {
         const myDb = db;
         myDb.destinos.add(d);
         console.log('Todos los destinos almacenados en la db!');
         this.store.dispatch(new NuevoDestinoAction(d));
         myDb.destinos.toArray().then(destinos => console.log(destinos));
       }
     });
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  elegir(d: DestinoViaje) {
    // this.destinos.forEach((x) => x.setSelected(false));
    // d.setSelected(true);
    // this.current.next(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  // subcribeOnChance(fn) {
  //   this.current.subscribe(fn);
  // }
}
