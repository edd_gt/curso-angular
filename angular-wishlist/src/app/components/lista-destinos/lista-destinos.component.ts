import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.models';
// import { DestinosViajesState } from './../models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers:[DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  // destinos: DestinoViaje[];
  updates: string[];

  constructor(
    public destinosApiClient: DestinosApiClient,
    public store: Store<AppState>
  ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit(): void {
     this.store
       .select((state) => state.destinos)
       .subscribe((data) => {
         let d = data.favorito;
         if (d != null) {
           this.updates.push('Se eligió: ' + d.nombre);
         }
       });
  }

  //   this.destinos.push(d);
    // agregado(d: DestinoViaje): boolean {
  //   console.log(this.destinos);
  //   return false;
  // }

  agregado(d: DestinoViaje): boolean {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  // elegido(d: DestinoViaje) {
  //   this.destinos.forEach(function (x) {
  //     x.setSelected(false);
  //   });

  //   d.setSelected(true);
  // }

  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }
}
