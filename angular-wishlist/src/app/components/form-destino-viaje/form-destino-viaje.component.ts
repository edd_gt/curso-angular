import {
  Component,
  EventEmitter,
  forwardRef,
  Inject,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { fromEvent } from 'rxjs';
import {
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { DestinoViaje } from '../../models/destino-viaje.models';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css'],
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  searchResults: string[];

  constructor(
    fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
  ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: [
        '',
        Validators.compose([
          Validators.required,
          this.nombreValidator,
          this.nombreValidatorParametrizable(this.minLongitud),
        ]),
      ],
      url: [''],
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario: ', form);
    });
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        //Obtiene el valor del imput cada vez que se presiona una tecla
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        //filtra que el valor de input sea mayor a 3 caracteres sino, no continua
        filter((text) => text.length > 2),
        //Espera 200 milisegundos para validar si se ingreso algo más o hubo un cambio
        debounceTime(200),
        //Verifica que despues de los 200 milisegundos cambió algo
        distinctUntilChanged(),
        switchMap((text: string) =>
          ajax(this.config.apiEndpoint + '/ciudades?q=' + text)
        )
      )
      .subscribe((ajaxResponse) => {
        let resp: string[] = ajaxResponse.response;
        this.searchResults = resp.filter(
          (x) =>
            x
              .toLocaleUpperCase()
              .indexOf(elemNombre.value.toLocaleUpperCase()) > -1
        );
        // console.log(resp);
        // console.log(this.searchResults);
      });
  }

  guardar(nombre: string, url: string): boolean {
    let d = new DestinoViaje(nombre, url);

    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;

    if (l > 0 && l < 5) {
      return { invalidNombre: true };
    }

    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;

      if (l > 0 && l < minLong) {
        return { minLongNombre: true };
      }

      return null;
    };
  }
}
