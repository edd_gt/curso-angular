import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import './polyfills';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .then((ref) => {
    if(window['ngRef'])
    {
      window['ngRef'].destroy();
    }
    window['ngRef'].destroy();
  })
  .catch((err) => console.error(err));
